const electron = require('electron')
// const {ipcMain} = require('electron')  

// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
//  mainWindow = new BrowserWindow({width: 800, height: 600})
	  mainWindow = new BrowserWindow({
      kiosk: true,
      webSecurity: false,
      webPreferences: {
        nodeIntegration: true
      }
    });



  mainWindow.loadURL(`file://${__dirname}/index.html`)

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

// ipcMain.on('openFile', (event, path) => { 
//   const {dialog} = require('electron') 
//   const fs = require('fs') 
//   dialog.showOpenDialog(function (fileNames) { 
     
//      // fileNames is an array that contains all the selected 
//      if(fileNames === undefined) { 
//         console.log("No file selected"); 
     
//      } else { 
//         readFile(fileNames[0]); 
//      } 
//   });
// });