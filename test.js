// const { ipcMain, dialog, app } = require('electron')
const {dialog} = require('electron').remote;
const { BrowserWindow } = require('electron').remote

var currPattern = 0;

// const {dialog} = require('electron').remote;

// document.querySelector('#selectBtn').addEventListener('click', function (event) {
//     dialog.showOpenDialog({
//         properties: ['openFile', 'multiSelections']
//     }, function (files) {
//         if (files !== undefined) {
//             // handle files
//         }
//     });
// });




var song = {
	"name": "default example song",
	"bpm": 123,
	pattern: [],
	bar: [],
	instrument: [
		{ "data": new Audio('audio/kit/ride.wav'), "name": "ride" },
		{ "data": new Audio('audio/kit/hihatopen.wav'), "name": "hihat open" },
		{ "data": new Audio('audio/kit/hihatclosed.wav'), "name": "hihat closed" }, 
		{ "data": new Audio('audio/kit/snare.wav'), "name": "snare" },
		{ "data": new Audio('audio/kit/kick.wav'), "name": "kick" }
	]
}

new_pattern();

function render_pattern() {
	for (let i=0; i<song.instrument.length; i++) {
		var d = `
		<div class="instRow flex-container">
			<div class="instInfo">${song.instrument[i].name}</div>
			<div class="patternRow flex-container" id="${i}">
				<div class="quant16 fourBeat" id="0"></div>
				<div class="quant16 sixteenBeat" id="1"></div>
				<div class="quant16 eightBeat" id="2"></div>
				<div class="quant16 sixteenBeat" id="3"></div>
				<div class="quant16 fourBeat" id="4"></div>
				<div class="quant16 sixteenBeat" id="5"></div>
				<div class="quant16 eightBeat" id="6"></div>
				<div class="quant16 sixteenBeat" id="7"></div>
				<div class="quant16 fourBeat" id="8"></div>
				<div class="quant16 sixteenBeat" id="9"></div>
				<div class="quant16 eightBeat" id="10"></div>
				<div class="quant16 sixteenBeat" id="11"></div>
				<div class="quant16 fourBeat" id="12"></div>
				<div class="quant16 sixteenBeat" id="13"></div>
				<div class="quant16 eightBeat" id="14"></div>
				<div class="quant16 sixteenBeat" id="15"></div>
			</div>		
		</div>`;	
		$(".patternEditorGrid").append(d);
	};


	// Attack listeners to each 16th beat div
	for(let i = 0; i < document.getElementsByClassName("quant16").length; i++) {
		document.getElementsByClassName("quant16")[i].addEventListener("click", function() {
			row = this.parentElement.id;
			col = this.id;
			this.classList.toggle("active");
			let currentState = song.pattern[currPattern].inst[row].beat[col].trigger;
			song.pattern[currPattern].inst[row].beat[col].trigger = !currentState;
		})
	}	

}

function next_pattern() {
	currPattern++;
	if (currPattern == 0) {
		$("#btn_prev_bar").attr("disabled", true);
	} else {
		$("#btn_prev_bar").attr("disabled", false);
	}	
	if (currPattern >= song.pattern.length-1) {
		$("#btn_next_bar").attr("disabled", true);
	} else {
		$("#btn_next_bar").attr("disabled", false);
	}
	$("#currPattern").html(currPattern);

}

function prev_pattern() {
	console.log(song.pattern.length);
	if (currPattern > 0) {
		currPattern--;
	} 
	if (currPattern == 0) {
		$("#btn_prev_bar").attr("disabled", true);
	} else {
		$("#btn_prev_bar").attr("disabled", false);
	}	
	if (currPattern >= song.pattern.length-1) {
		$("#btn_next_bar").attr("disabled", true);
	} else {
		$("#btn_next_bar").attr("disabled", false);
	}
	$("#currPattern").html(currPattern);
}

function debug() {
	console.log(song);
}

function new_pattern() {

	var pattern = {
		name: "newpattern",
		bpm: 0,
		q: 16,
		inst: [
			{ "beat": [] }
		]
	};

	for (instc = 0; instc < 5; instc++) {
		var beat = {"beat": []};
		pattern.inst.push(beat);
		for (q = 0; q < 16; q++) {
			var trigger = {"trigger": false, "volume": 100};
			pattern.inst[instc].beat.push(trigger);
		}
	}

	song.pattern.push(pattern);

	render_pattern();
}


async function loop_pattern() {

	var bpm = 120;
	var pausetime = 1000 / (bpm/60) / 4;

	while (true) {
		for (let t=0 ; t<16; t++) {
			for (let i=0; i<5; i++) {
				// console.log(bar.inst[i].beat[t].trigger);
				if (song.pattern[currPattern].inst[i].beat[t].trigger) {
					var d = song.instrument[i].data.cloneNode();
					d.play();
				}	
			}
			// TODO: Better visualisation of where in pattern we are
			$("#"+t).addClass("playing");
			await sleep(pausetime);
		}
	}

}

function play_all() {
	// TODO: All this
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
  }

function open_song() {
	// var remote = electron.remote;
	// var dialog = remote.dialog;
	// dialog.showOpenDialogSync(mainWindow, {
	console.log(dialog.showOpenDialogSync({
			properties: ['openFile', 'openDirectory']
	}));
}

function save_song() {
	var options = {
		"properties": []
	}

	var r = dialog.showSaveDialogSync(BrowserWindow.getFocusedWindow(), options);
	console.log(r);
}